const findText = require("./findText");
const getFileContent = require("./getFileContent");

jest.mock("./getFileContent");

describe("findText", function () {
  const filePath = "example.txt";
  const textToFind = "то что нужно";

  it("should return true if the text is found in the file content", async () => {
    const fileContent = "в этом файле есть то что нужно";
    getFileContent.mockResolvedValue(fileContent);
    const result = await findText(filePath, textToFind);
    // expect(findText).toBeCalledTimes(1);
    expect(result).toBe(true);
  });

  it("should return false if the text isn't found in the file content", async () => {
    const fileContent = "в этом файле нет того что нужно";
    getFileContent.mockResolvedValue(fileContent); // Resolved для промиса или Return для синхроннойф-ции
    const result = await findText(filePath, textToFind);
    expect(result).toBe(false);
  });

  it("should handle errors when reading file", async () => {
    const error = new Error("File not found");
    getFileContent.mockRejectedValue(error);

    await expect(findText(filePath, textToFind)).rejects.toThrow(error);
  });
});
