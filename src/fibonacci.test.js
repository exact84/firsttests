const getFibonacci = require("./fibonacci");

describe("fibonacci", function () {
  test("should return 0 for n = 0", () => {
    expect(getFibonacci(0)).toBe(0);
  });

  test("should return 1 for n = 1", () => {
    expect(getFibonacci(1)).toBe(1);
  });

  test("should return 1 for n = 2", () => {
    expect(getFibonacci(2)).toBe(1);
  });

  test("should return 8 for n = 6", () => {
    expect(getFibonacci(6)).toBe(8);
  });
});
