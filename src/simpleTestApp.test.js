const startServer = require("./simpleTestApp");
const supertest = require("supertest");

let server;

beforeAll(async () => {
  // Запуск сервера
  server = await startServer();
});

afterAll(async () => {
  // Остановка сервера
  if (server) {
    await server.close();
  }
});

describe("simpleTestApp", function () {
  it("GET should return an empty array of tasks", async () => {
    const res = await supertest(server).get("/");
    expect(res.status).toBe(200);
    expect(res.body).toEqual([]);
  });

  it("POST should return 400 if title is missing", async () => {
    const res = await supertest(server)
      .post("/")
      .send({ description: "Task description" });
    expect(res.status).toBe(400);
    expect(res.body.errors[0].msg).toBe("Invalid value");
  });

  it("POST should return 400 if description is missing", async () => {
    const res = await supertest(server).post("/").send({ title: "Task title" });
    expect(res.status).toBe(400);
    expect(res.body.errors[0].msg).toBe("Invalid value");
  });

  it("POST should return 400 if title is shorter than 3 letters", async () => {
    const res = await supertest(server)
      .post("/")
      .send({ title: "ab", description: "Task description" });
    expect(res.status).toBe(400);
    expect(res.body.errors[0].msg).toBe(
      "Minimal length for task name is 3 letter!"
    );
  });

  it("POST should add a task and return 200 if request body is valid", async () => {
    const newTask = { title: "New Task", description: "New task description" };
    const res = await supertest(server).post("/").send(newTask);
    expect(res.status).toBe(200);

    const getRes = await supertest(server).get("/");
    expect(getRes.body).toContainEqual(newTask);
  });
});
